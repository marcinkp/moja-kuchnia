package pk.mojakuchnia.data.realm;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Legion on 15.12.2017.
 */

public class RealmRecipe extends RealmObject {
    @PrimaryKey
    private Long id; /**unique*/
    private String recipeName;

    @Required
    private String recipe;
    private RealmList<RealmIngredient> ingredientList;
    private int timeNeeded; /**in minutes */

    /**
     * Getters and Setters
     */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public String getRecipe() {
        return recipe;
    }

    public void setRecipe(String recipe) {
        this.recipe = recipe;
    }

    public int getTimeNeeded() {
        return timeNeeded;
    }

    public void setTimeNeeded(int timeNeeded) {
        this.timeNeeded = timeNeeded;
    }

    public RealmList<RealmIngredient> getIngredientList() {
        return ingredientList;
    }

    public void setIngredientList(RealmList<RealmIngredient> ingredientList) {
        this.ingredientList = ingredientList;
    }


    /**
     * Methods
     */

    /**
     * Adding Ingriedent to list
     * @param ingredient
     */

    public void addIngredient(RealmIngredient ingredient){
        ingredientList.add(ingredient);
    }
}
