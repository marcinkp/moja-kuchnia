package pk.mojakuchnia.data.realm;

import io.realm.RealmObject;

/**
 * Created by Legion on 16.12.2017.
 */

public class RealmIngredient extends RealmObject {
    private String name;
    /*
    This field contains String returned by deleteCategoryWordsFromIngredientName (see RecipeDao)
     */
    private String nameToSearchByWhileSearchingByProductWithACategory;
    private String category;
    private String quantity;

    public RealmIngredient() {

    }

    public RealmIngredient(String name, String quantity) {
        this.name = name;
        this.quantity = quantity;
    }

    public RealmIngredient(String name, String category, String nameToSearchByWhileSearchingByProductWithACategory, String quantity) {
        this.name = name;
        this.nameToSearchByWhileSearchingByProductWithACategory = nameToSearchByWhileSearchingByProductWithACategory;
        this.category = category;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameToSearchByWhileSearchingByProductWithACategory() {
        return nameToSearchByWhileSearchingByProductWithACategory;
    }

    public void setNameToSearchByWhileSearchingByProductWithACategory(String nameToSearchByWhileSearchingByProductWithACategory) {
        this.nameToSearchByWhileSearchingByProductWithACategory = nameToSearchByWhileSearchingByProductWithACategory;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }


}
