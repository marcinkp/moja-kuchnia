package pk.mojakuchnia.data.realm;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by marci on 13.12.2017.
 */

public class RealmUserProduct extends RealmObject {
    @PrimaryKey
    private Long id;
    private long globalTradeItemNumber;
    private String brandOwner;
    private String manufacturerName;
    private String brandName;
    @Required
    private String productName;
    private String category;
    private String description;
    private Float kiloCaloriesPerHundredGrams;
    private Float fatPerHundredGrams;
    private Float carbohydratesPerHundredGrams;
    private Float proteinPerHundredGrams;
    private RealmList<String> allergens;
    private String countryOfOrigin;
    private String pictureName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getGlobalTradeItemNumber() {
        return globalTradeItemNumber;
    }

    public void setGlobalTradeItemNumber(long globalTradeItemNumber) {
        this.globalTradeItemNumber = globalTradeItemNumber;
    }

    public String getBrandOwner() {
        return brandOwner;
    }

    public void setBrandOwner(String brandOwner) {
        this.brandOwner = brandOwner;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    public String getPictureName() {
        return pictureName;
    }

    public void setPictureName(String pictureName) {
        this.pictureName = pictureName;
    }

    public Float getKiloCaloriesPerHundredGrams() {
        return kiloCaloriesPerHundredGrams;
    }

    public void setKiloCaloriesPerHundredGrams(Float kiloCaloriesPerHundredGrams) {
        this.kiloCaloriesPerHundredGrams = kiloCaloriesPerHundredGrams;
    }

    public Float getFatPerHundredGrams() {
        return fatPerHundredGrams;
    }

    public void setFatPerHundredGrams(Float fatPerHundredGrams) {
        this.fatPerHundredGrams = fatPerHundredGrams;
    }

    public Float getCarbohydratesPerHundredGrams() {
        return carbohydratesPerHundredGrams;
    }

    public void setCarbohydratesPerHundredGrams(Float carbohydratesPerHundredGrams) {
        this.carbohydratesPerHundredGrams = carbohydratesPerHundredGrams;
    }

    public Float getProteinPerHundredGrams() {
        return proteinPerHundredGrams;
    }

    public void setProteinPerHundredGrams(Float proteinPerHundredGrams) {
        this.proteinPerHundredGrams = proteinPerHundredGrams;
    }

    public RealmList<String> getAllergens() {
        return allergens;
    }

    public void setAllergens(RealmList<String> allergens) {
        this.allergens = allergens;
    }
}


