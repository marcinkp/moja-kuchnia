package pk.mojakuchnia.data.realm;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by marci on 14.02.2018.
 */

public class RealmShoppingList extends RealmObject {
    @PrimaryKey
    private Long id;
    @Required
    private RealmList<RealmIngredient> ingredientList;
    @Required
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<RealmIngredient> getIngredientList() {
        return ingredientList;
    }

    public void setIngredientList(RealmList<RealmIngredient> ingredientList) {
        this.ingredientList = ingredientList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
