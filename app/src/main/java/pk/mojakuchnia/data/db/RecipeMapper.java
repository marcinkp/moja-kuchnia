package pk.mojakuchnia.data.db;

import pk.mojakuchnia.data.pojo.Ingredient;
import pk.mojakuchnia.data.pojo.Recipe;
import pk.mojakuchnia.data.realm.RealmIngredient;
import pk.mojakuchnia.data.realm.RealmRecipe;

/**
 * Created by Legion on 16.12.2017.
 */

final class RecipeMapper {
    private RecipeMapper(){
    }

    static Recipe fromRealm(RealmRecipe realmRecipe){
        Recipe recipe = new Recipe();
        recipe.setId(realmRecipe.getId());
        recipe.setRecipeName(realmRecipe.getRecipeName());

        recipe.setRecipe(realmRecipe.getRecipe());
        recipe.setTimeNeeded(realmRecipe.getTimeNeeded());

        for(RealmIngredient realmIngredient : realmRecipe.getIngredientList()){
            recipe.addIngredient(new Ingredient(realmIngredient.getName(), realmIngredient.getCategory(), realmIngredient.getQuantity()));
        }
        return recipe;
    }
}
