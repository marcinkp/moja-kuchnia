package pk.mojakuchnia.data.db;

import android.content.Context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import pk.mojakuchnia.data.pojo.Ingredient;
import pk.mojakuchnia.data.pojo.Product;
import pk.mojakuchnia.data.pojo.Recipe;
import pk.mojakuchnia.data.realm.RealmIngredient;
import pk.mojakuchnia.data.realm.RealmRecipe;


/**
 * Created by Legion on 15.12.2017.
 */

public class RecipeDao {
    private Realm realm;
    private RealmConfiguration realmConfiguration;
    private final static String DATABASE_FILENAME = "recipes.realm";
    Context context;

    public RecipeDao(Context context) {
        Realm.init(context);
        this.context = context;
        realmConfiguration = new RealmConfiguration.Builder().name(DATABASE_FILENAME).directory(context.getExternalFilesDir(null)).build();
        realm = Realm.getInstance(realmConfiguration);
    }

    public void close() {
        realm.close();
    }


    public void insertRecipe(Recipe recipeToInsert){
        realm.beginTransaction();

        RealmRecipe realmRecipe = realm.createObject(RealmRecipe.class, generateId());
        realmRecipe.setRecipeName(recipeToInsert.getRecipeName());
        realmRecipe.setRecipe(recipeToInsert.getRecipe());
        realmRecipe.setTimeNeeded(recipeToInsert.getTimeNeeded());

        RealmList<RealmIngredient> realmIngredientList = new RealmList<>();
        for(Ingredient ingredient : recipeToInsert.getIngredientList()) {
            RealmIngredient realmIngredient = realm.createObject(RealmIngredient.class);
            realmIngredient.setName(ingredient.getName());
            if (ingredient.getCategory() != null) {
                realmIngredient.setNameToSearchByWhileSearchingByProductWithACategory(deleteCategoryWordsFromIngredientName(ingredient.getName(), ingredient.getCategory()));
            }
            else {
                realmIngredient.setNameToSearchByWhileSearchingByProductWithACategory(ingredient.getName());
            }
            realmIngredient.setCategory(ingredient.getCategory());
            realmIngredient.setQuantity(ingredient.getQuantity());

            realmIngredientList.add(realmIngredient);
        }

        realmRecipe.setIngredientList(realmIngredientList);

        realm.commitTransaction();
    }

    /*
    Helper method for getByListOfProducts
    */
    private boolean containsLikeIgnoreCase(List<String> list, String soughtFor) {
        for (String current : list) {
            if (current.toLowerCase().contains(soughtFor.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    /*
    This method deletes all words from ingredient name that also exist in a category of the ingredient.
    This is done only if the name contains some other words besides ones from the category.
    The purpose is to improve the accuracy of results.
    Example:
    When the category of ingredient is "Jabłko" and the name is "Jabłko antonówka", "Jabłko" word will be deleted.
    This will let an algorithm to find the recipe by more precise word which is "antonówka".
     */
    private String deleteCategoryWordsFromIngredientName(String name, String category) {
        List<String> tokens = new ArrayList<>();
        tokens.addAll(Arrays.asList(name.split(" ")));
        List<String> categoryWords = Arrays.asList(category.split(" "));
        boolean foundNonSharedWord = false;
        for (int i = 0 ; i < tokens.size() ; i++) {
            System.out.println(tokens.get(i));
            if(!(categoryWords.contains(tokens.get(i)))) {
                foundNonSharedWord = true;
            }
        }
        if(!foundNonSharedWord) {
            return name;
        }
        else {
            for (int i = 0 ; i < tokens.size() ; i++) {
                if(categoryWords.contains(tokens.get(i))) {
                    tokens.remove(i);
                    i--;
                }
            }
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (String token : tokens) {
            stringBuilder.append(token).append(" ");
        }
        return stringBuilder.toString();
    }

    /*
    This method deletes last character of every token with length > 3 to not differentiate between singular and plural form while searching.
    Such process is called 'Stemming'.
    Example:
    "Jabłko" and "Jabłka" will both be reduced to "Jabłk".
     */
    private List<String> stemTokens(List<String> tokens) {
        String token;
        for (int i = 0 ; i < tokens.size() ; i++) {
            if (tokens.get(i).length() > 3) {
                token = tokens.get(i).substring(0, tokens.get(i).length()-1);
                tokens.set(i, token);
            }
        }
        return tokens;
    }

    public List<Recipe> getByListOfProducts(List<Product> productList, int timeOfCooking){
        /*
        This list contains returned recipes matching to query.
         */
        List<Recipe> recipeList = new ArrayList<>();
        /*
        This list contains list of words called *tokens*, taken from product name and description.
        First element of that list is a category of product.
         */
        List<List<String>> tokenListOfListsOfProductsWithCategory = new ArrayList<>();
        /*
        As above but not stemmed. Will be used during removal of recipes from found results without matching tokens (but with matching category).
         */
        List<List<String>> nonStemmedTokenListOfListsOfProductsWithCategory = new ArrayList<>();
        /*
        This list contains list of words called *tokens* taken from product name and description.
         */
        List<List<String>> tokenListOfListsOfProductsWithoutCategory = new ArrayList<>();

        /*
        Create token lists
         */
        for (Product product : productList) {
            List<String> tokenList = new ArrayList<>();

            List<String> tempTokenList = new ArrayList<>();
            List<String> tempTokenListForNonStemmedList = new ArrayList<>();
            tempTokenList.addAll(Arrays.asList((product.getProductName() + " " + product.getDescription()).split(" ")));
            tempTokenListForNonStemmedList.addAll(Arrays.asList((product.getProductName() + " " + product.getDescription()).split(" ")));

            nonStemmedTokenListOfListsOfProductsWithCategory.add(tempTokenListForNonStemmedList);

            if (product.getCategory() != null) {
                tempTokenList = stemTokens(tempTokenList);
                tokenList.addAll(tempTokenList);
                tokenList.add(0, product.getCategory());
                tempTokenListForNonStemmedList.add(0, product.getCategory());
                tokenListOfListsOfProductsWithCategory.add(tokenList);
                nonStemmedTokenListOfListsOfProductsWithCategory.add(tempTokenListForNonStemmedList);
            }
            else {
                tempTokenList = stemTokens(tempTokenList);
                tokenList.addAll(tempTokenList);
                tokenListOfListsOfProductsWithoutCategory.add(tokenList);
            }
        }

        /*
        Finding recipes with ingredients matching by category
        */
        RealmResults<RealmRecipe> realmResult = null;
        if (!tokenListOfListsOfProductsWithCategory.isEmpty()) {
            RealmQuery<RealmRecipe> realmQuery = realm.where(RealmRecipe.class);
            if (timeOfCooking != 0) {
                realmQuery.lessThanOrEqualTo("timeNeeded", timeOfCooking);
            }
            for (List<String> tokenList : tokenListOfListsOfProductsWithCategory) {
                realmQuery.equalTo("ingredientList.category", tokenList.get(0));
            }
            realmResult = realmQuery.findAll();

            /*
            Finding recipes from found results without matching tokens
            */
            List<Integer> listOfResultIndexesToBeRemoved = new ArrayList<>();
            for (int i = 0 ; i < realmResult.size() ; i++) {
                RealmRecipe realmRecipe = realmResult.get(i);
                RealmList<RealmIngredient> ingredientList = realmRecipe.getIngredientList();

                /*
                Perform deep copy
                 */
                List<List<String>> tempTokenListOfLists = new ArrayList<>();
                for(List<String> tokenList : nonStemmedTokenListOfListsOfProductsWithCategory) {
                    tempTokenListOfLists.add(tokenList);
                }

                for (RealmIngredient realmIngredient : ingredientList) {
                    for (int j = tempTokenListOfLists.size() - 1 ; j >= 0 ; j--) {
                        List<String> tokenList = new ArrayList<>(tempTokenListOfLists.get(j));
                        List<String> tokenSubListWithoutCategory = tokenList.subList(1, tokenList.size());
                        List<String> stemmedTokenListOfNameToSearchBy = new ArrayList<>(Arrays.asList(realmIngredient.getNameToSearchByWhileSearchingByProductWithACategory().split(" ")));
                        stemmedTokenListOfNameToSearchBy = stemTokens(stemmedTokenListOfNameToSearchBy);
                        if (realmIngredient.getCategory() != null && realmIngredient.getCategory().equals(tokenList.get(0))) {
                            for (String ingredientToken : stemmedTokenListOfNameToSearchBy) {
                                if (containsLikeIgnoreCase(tokenSubListWithoutCategory, ingredientToken)) {
                                    if (tempTokenListOfLists.size() > 0) {
                                        tempTokenListOfLists.remove(j);
                                        break;
                                    }
                                    else {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                if(!tempTokenListOfLists.isEmpty()) {
                    listOfResultIndexesToBeRemoved.add(i);
                }
            }

            /*
            Converting RealmResult list to ArrayList
            Deleting recipes directly from RealmResult list would delete them from both database
            and list, that's why if we need to delete something from RealmResult we need to
            convert it to a typical list before. Further filtering of that list will have to
            be done manually.
            */

            for (RealmRecipe realmRecipe : realmResult) {
                recipeList.add(RecipeMapper.fromRealm(realmRecipe));
            }

            /*
            Deleting recipes without matching tokens
            */
            for (Integer index : listOfResultIndexesToBeRemoved) {
                recipeList.remove(index.intValue());
            }
        }

        /*
        Finding recipes by ingredients matching by tokens (for products without category)
         */
        if (!tokenListOfListsOfProductsWithoutCategory.isEmpty()) {
            RealmQuery<RealmRecipe> realmQuery;
            if(recipeList.isEmpty() && tokenListOfListsOfProductsWithCategory.isEmpty()) {
                realmQuery = realm.where(RealmRecipe.class);
                if (timeOfCooking != 0) {
                    realmQuery.lessThanOrEqualTo("timeNeeded", timeOfCooking);
                }
                for (List<String> tokenList : tokenListOfListsOfProductsWithoutCategory) {
                    realmQuery.beginGroup().like("ingredientList.name", "*" + tokenList.get(0) + "*", Case.INSENSITIVE);
                    for (String token : tokenList) {
                        realmQuery.or().like("ingredientList.name", "*" + token + "*", Case.INSENSITIVE);
                    }
                    realmQuery.endGroup();
                }
                realmResult = realmQuery.findAll();
                for (RealmRecipe realmRecipe : realmResult) {
                    recipeList.add(RecipeMapper.fromRealm(realmRecipe));
                }
            }
            else if (!recipeList.isEmpty()) {
                for (int i = recipeList.size() - 1 ; i >= 0 ; i--) {
                    List<Ingredient> ingredientList = recipeList.get(i).getIngredientList();
                    boolean foundToken = false;
                    for (List<String> tokenList : tokenListOfListsOfProductsWithoutCategory) {
                        for (String token : tokenList) {
                            for (Ingredient ingredient : ingredientList) {
                                if (ingredient.getName().toLowerCase().contains(token.toLowerCase())) {
                                    foundToken = true;
                                }
                            }
                        }
                    }
                    if (!foundToken) {
                        recipeList.remove(i);
                    }
                }
            }
        }

        return recipeList;
    }

    public List<Recipe> getBySearchQuery(String query, int timeOfCooking) {
        List<String> tokenList = Arrays.asList(query.replaceAll("^\"", "").split("\"?( |$)(?=(([^\"]*\"){2})*[^\"]*$)\"?"));
        tokenList = stemTokens(tokenList);

        RealmQuery<RealmRecipe> realmQuery = realm.where(RealmRecipe.class);
        if (timeOfCooking != 0) {
            realmQuery.lessThanOrEqualTo("timeNeeded", timeOfCooking);
        }
        for (String token : tokenList) {
            realmQuery.beginGroup();
            realmQuery.like("ingredientList.name", "*" + token + "*", Case.INSENSITIVE);
            realmQuery.or().like("ingredientList.category", "*" + token + "*", Case.INSENSITIVE);
            realmQuery.or().like("recipeName", "*" + token + "*", Case.INSENSITIVE);
            realmQuery.or().like("recipe", "*" + token + "*", Case.INSENSITIVE);
            realmQuery.endGroup();
        }

        RealmResults<RealmRecipe> realmResult = realmQuery.findAll();
        List<Recipe> recipeList = new ArrayList<>();
        for (RealmRecipe realmRecipe : realmResult) {
            recipeList.add(RecipeMapper.fromRealm(realmRecipe));
        }
        return recipeList;
    }

    public Recipe getRandomRecipe() {
        /*
        Will crash when max id is bigger than 2147483647.. Implying it would ever happen.
        Unfortunately, Random class' nextLong method doesn't take a range parameter.
         */
        int id = realm.where(RealmRecipe.class).max("id").intValue();
        return RecipeMapper.fromRealm(realm.where(RealmRecipe.class).equalTo("id", Long.valueOf(new Random().nextInt(id + 1))).findFirst());
    }

    public Recipe getRecipeByID(Long id) {
        return RecipeMapper.fromRealm(realm.where(RealmRecipe.class).equalTo("id", id).findFirst());
    }

    public List<Recipe> getAllRecipes() {
        List<Recipe> recipeList = new ArrayList<>();
        RealmResults<RealmRecipe> realmResult = realm.where(RealmRecipe.class).findAll();
        for (RealmRecipe realmRecipe : realmResult) {
            recipeList.add(RecipeMapper.fromRealm(realmRecipe));
        }
        return recipeList;
    }

    private Long generateId() {
        Number id = realm.where(RealmRecipe.class).max("id");
        if (id != null) {
            return (Long) id + 1L;
        }
        else return 1L;
    }

}
