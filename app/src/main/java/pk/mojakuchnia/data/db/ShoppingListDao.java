package pk.mojakuchnia.data.db;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;
import pk.mojakuchnia.data.pojo.Ingredient;
import pk.mojakuchnia.data.pojo.ShoppingList;
import pk.mojakuchnia.data.realm.RealmIngredient;
import pk.mojakuchnia.data.realm.RealmShoppingList;

/**
 * Created by marci on 14.02.2018.
 */

public class ShoppingListDao {
    private Realm realm;
    private RealmConfiguration realmConfig;
    private final static String DATABASE_FILENAME = "shopping_list.realm";

    public ShoppingListDao(Context context) {
        Realm.init(context);
        realmConfig = new RealmConfiguration.Builder().name(DATABASE_FILENAME).build();
        realm = Realm.getInstance(realmConfig);
    }

    public void close() {
        realm.close();
    }

    public void insertShoppingList(final ShoppingList shoppingList) {
        realm.beginTransaction();

        RealmShoppingList realmShoppingList = realm.createObject(RealmShoppingList.class, generateId());
        realmShoppingList.setName(shoppingList.getName());

        RealmList<RealmIngredient> realmIngredientList = new RealmList<>();
        for(Ingredient ingredient : shoppingList.getIngredientList()) {
            RealmIngredient realmIngredient = realm.createObject(RealmIngredient.class);
            realmIngredient.setName(ingredient.getName());
            realmIngredient.setCategory(ingredient.getCategory());
            realmIngredient.setQuantity(ingredient.getQuantity());

            realmIngredientList.add(realmIngredient);
        }

        realmShoppingList.setIngredientList(realmIngredientList);

        realm.commitTransaction();
    }

    public void updateShoppingList(final ShoppingList shoppingList) {
        final RealmShoppingList realmShoppingList = new RealmShoppingList();
        realmShoppingList.setName(shoppingList.getName());

        RealmList<RealmIngredient> realmIngredientList = new RealmList<>();
        for(Ingredient ingredient : shoppingList.getIngredientList()) {
            RealmIngredient realmIngredient = realm.createObject(RealmIngredient.class);
            realmIngredient.setName(ingredient.getName());
            realmIngredient.setCategory(ingredient.getCategory());
            realmIngredient.setQuantity(ingredient.getQuantity());

            realmIngredientList.add(realmIngredient);
        }

        realmShoppingList.setIngredientList(realmIngredientList);

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                // This will update an existing object with the same primary key
                // or create a new object if an object with no primary key = 42
                realm.copyToRealmOrUpdate(realmShoppingList);
            }
        });
    }

    public void deleteAllShoppingLists() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }

    public void deleteShoppingList(Long id) {
        final RealmResults<RealmShoppingList> shoppingListToBeDeleted = realm.where(RealmShoppingList.class).equalTo("id", id).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                shoppingListToBeDeleted.deleteFirstFromRealm();
            }
        });
    }

    public List<ShoppingList> getAllShoppingLists() {
        List<ShoppingList> listOfShoppingLists = new ArrayList<>();
        RealmResults<RealmShoppingList> returnedRealmShoppingLists = realm.where(RealmShoppingList.class).findAll();
        for (RealmShoppingList realmShoppingList : returnedRealmShoppingLists) {
            listOfShoppingLists.add(ShoppingListMapper.fromRealm(realmShoppingList));
        }
        return listOfShoppingLists;
    }

    private Long generateId() {
        Number id = realm.where(RealmShoppingList.class).max("id");
        if (id != null) {
            return (Long) id + 1L;
        }
        else return 1L;
    }
}
