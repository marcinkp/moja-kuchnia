package pk.mojakuchnia.data.db;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;
import pk.mojakuchnia.data.pojo.Product;
import pk.mojakuchnia.data.realm.RealmProduct;
import pk.mojakuchnia.data.realm.RealmUserProduct;

/**
 * Created by marci on 14.12.2017.
 */

public class UserProductDao {
    private Realm realm;
    private RealmConfiguration realmConfig;
    private final static String DATABASE_FILENAME = "user_products.realm";

    public UserProductDao(Context context) {
        Realm.init(context);
        realmConfig = new RealmConfiguration.Builder().name(DATABASE_FILENAME).build();
        realm = Realm.getInstance(realmConfig);
    }

    public void close() {
        realm.close();
    }

    public void insertProduct(final Product product) {
        realm.beginTransaction();

        RealmUserProduct realmUserProduct = realm.createObject(RealmUserProduct.class, generateId());
        realmUserProduct.setGlobalTradeItemNumber(product.getGlobalTradeItemNumber());
        realmUserProduct.setBrandOwner(product.getBrandOwner());
        realmUserProduct.setManufacturerName(product.getManufacturerName());
        realmUserProduct.setBrandName(product.getBrandName());
        realmUserProduct.setProductName(product.getProductName());
        realmUserProduct.setCategory(product.getCategory());
        realmUserProduct.setDescription(product.getDescription());
        realmUserProduct.setKiloCaloriesPerHundredGrams(product.getKiloCaloriesPerHundredGrams());
        realmUserProduct.setFatPerHundredGrams(product.getFatPerHundredGrams());
        realmUserProduct.setCarbohydratesPerHundredGrams(product.getCarbohydratesPerHundredGrams());
        realmUserProduct.setProteinPerHundredGrams(product.getProteinPerHundredGrams());

        if(product.getAllergens() != null) {
            RealmList<String> allergens = new RealmList<>();
            allergens.addAll(product.getAllergens());
            realmUserProduct.setAllergens(allergens);
        }

        realmUserProduct.setCountryOfOrigin(product.getCountryOfOrigin());
        realmUserProduct.setPictureName(product.getPictureName());

        realm.commitTransaction();
    }

    public void deleteAllProducts() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }

    public void deleteProduct(Long id) {
        final RealmResults<RealmUserProduct> userProductToBeDeleted = realm.where(RealmUserProduct.class).equalTo("id", id).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                userProductToBeDeleted.deleteFirstFromRealm();
            }
        });
    }

    public List<Product> getAllProducts() {
        List<Product> products = new ArrayList<>();
        RealmResults<RealmUserProduct> returnedRealmUserProducts = realm.where(RealmUserProduct.class).findAll();
        for (RealmUserProduct realmUserProduct : returnedRealmUserProducts) {
            products.add(ProductMapper.fromRealm(realmUserProduct));
        }
        return products;
    }

    private Long generateId() {
        Number id = realm.where(RealmUserProduct.class).max("id");
        if (id != null) {
            return (Long) id + 1L;
        }
        else return 1L;
    }
}

