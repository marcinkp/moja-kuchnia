package pk.mojakuchnia.data.db;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import pk.mojakuchnia.data.pojo.Product;
import pk.mojakuchnia.data.realm.RealmProduct;

/**
 * Created by marci on 10.12.2017.
 */

public class ProductDao {
    private Realm realm;
    private RealmConfiguration realmConfig;
    private final static String DATABASE_FILENAME = "products.realm";

    public ProductDao(Context context) {
        Realm.init(context);
        realmConfig = new RealmConfiguration.Builder().name(DATABASE_FILENAME).directory(context.getExternalFilesDir(null)).build();
        realm = Realm.getInstance(realmConfig);
    }

    public void close() {
        realm.close();
    }

    public void insertProduct(final Product product) {
        realm.beginTransaction();

        RealmProduct realmProduct = realm.createObject(RealmProduct.class, product.getGlobalTradeItemNumber());
        realmProduct.setBrandOwner(product.getBrandOwner());
        realmProduct.setManufacturerName(product.getManufacturerName());
        realmProduct.setBrandName(product.getBrandName());
        realmProduct.setProductName(product.getProductName());
        realmProduct.setCategory(product.getCategory());
        realmProduct.setDescription(product.getDescription());
        realmProduct.setKiloCaloriesPerHundredGrams(product.getKiloCaloriesPerHundredGrams());
        realmProduct.setFatPerHundredGrams(product.getFatPerHundredGrams());
        realmProduct.setCarbohydratesPerHundredGrams(product.getCarbohydratesPerHundredGrams());
        realmProduct.setProteinPerHundredGrams(product.getProteinPerHundredGrams());

        if(product.getAllergens() != null) {
            RealmList<String> allergens = new RealmList<>();
            allergens.addAll(product.getAllergens());
            realmProduct.setAllergens(allergens);
        }

        realmProduct.setCountryOfOrigin(product.getCountryOfOrigin());
        realmProduct.setPictureName(product.getPictureName());

        realm.commitTransaction();
    }

    public Product getProductByGtin(long globalTradeItemNumber) {
        return ProductMapper.fromRealm(realm.where(RealmProduct.class).equalTo("globalTradeItemNumber", globalTradeItemNumber).findFirst());
    }
}
