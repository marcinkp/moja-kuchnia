package pk.mojakuchnia.data.db;

import pk.mojakuchnia.data.pojo.Product;
import pk.mojakuchnia.data.realm.RealmProduct;
import pk.mojakuchnia.data.realm.RealmUserProduct;

/**
 * Created by marci on 10.12.2017.
 */

final class ProductMapper {
    private ProductMapper() {

    }

    static Product fromRealm(RealmProduct realmProduct) {
        if (realmProduct == null) {
            return null;
        }
        Product product = new Product();

        product.setGlobalTradeItemNumber(realmProduct.getGlobalTradeItemNumber());
        product.setBrandOwner(realmProduct.getBrandOwner());
        product.setManufacturerName(realmProduct.getManufacturerName());
        product.setBrandName(realmProduct.getBrandName());
        product.setProductName(realmProduct.getProductName());
        product.setCategory(realmProduct.getCategory());
        product.setDescription(realmProduct.getDescription());
        product.setKiloCaloriesPerHundredGrams(realmProduct.getKiloCaloriesPerHundredGrams());
        product.setFatPerHundredGrams(realmProduct.getFatPerHundredGrams());
        product.setCarbohydratesPerHundredGrams(realmProduct.getCarbohydratesPerHundredGrams());
        product.setProteinPerHundredGrams(realmProduct.getProteinPerHundredGrams());
        product.setAllergens(realmProduct.getAllergens());
        product.setCountryOfOrigin(realmProduct.getCountryOfOrigin());
        product.setPictureName(realmProduct.getPictureName());

        return product;
    }

    static Product fromRealm(RealmUserProduct realmUserProduct) {
        if (realmUserProduct == null) {
            return null;
        }
        Product product = new Product();

        product.setId(realmUserProduct.getId());
        product.setGlobalTradeItemNumber(realmUserProduct.getGlobalTradeItemNumber());
        product.setBrandOwner(realmUserProduct.getBrandOwner());
        product.setManufacturerName(realmUserProduct.getManufacturerName());
        product.setBrandName(realmUserProduct.getBrandName());
        product.setProductName(realmUserProduct.getProductName());
        product.setCategory(realmUserProduct.getCategory());
        product.setDescription(realmUserProduct.getDescription());
        product.setKiloCaloriesPerHundredGrams(realmUserProduct.getKiloCaloriesPerHundredGrams());
        product.setFatPerHundredGrams(realmUserProduct.getFatPerHundredGrams());
        product.setCarbohydratesPerHundredGrams(realmUserProduct.getCarbohydratesPerHundredGrams());
        product.setProteinPerHundredGrams(realmUserProduct.getProteinPerHundredGrams());
        product.setAllergens(realmUserProduct.getAllergens());
        product.setCountryOfOrigin(realmUserProduct.getCountryOfOrigin());
        product.setPictureName(realmUserProduct.getPictureName());

        return product;
    }
}
