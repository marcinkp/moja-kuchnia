package pk.mojakuchnia.data.db;

import pk.mojakuchnia.data.pojo.Ingredient;
import pk.mojakuchnia.data.pojo.ShoppingList;
import pk.mojakuchnia.data.realm.RealmIngredient;
import pk.mojakuchnia.data.realm.RealmShoppingList;

/**
 * Created by marci on 14.02.2018.
 */

final class ShoppingListMapper {
    private ShoppingListMapper() {

    }
    static ShoppingList fromRealm(RealmShoppingList realmShoppingList) {
        if(realmShoppingList == null) {
            return null;
        }

        ShoppingList shoppingList = new ShoppingList();
        shoppingList.setId(realmShoppingList.getId());
        shoppingList.setName(realmShoppingList.getName());
        for(RealmIngredient realmIngredient : realmShoppingList.getIngredientList()){
            shoppingList.addIngredient(new Ingredient(realmIngredient.getName(), realmIngredient.getCategory(), realmIngredient.getQuantity()));
        }
        return shoppingList;
    }
}
