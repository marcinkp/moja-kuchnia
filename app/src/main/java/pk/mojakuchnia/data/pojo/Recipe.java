package pk.mojakuchnia.data.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Legion on 15.12.2017.
 */

public class Recipe {

    private Long id; /**unique*/
    private String recipeName;
    private String recipe;

    private List<Ingredient> ingredientList;
    private int timeNeeded; /**in minutes */

    public Recipe() {
        ingredientList = new ArrayList<>();
    }

    /**
     * Getters and Setters
     */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public String getRecipe() {
        return recipe;
    }

    public void setRecipe(String recipe) {
        this.recipe = recipe;
    }

    public int getTimeNeeded() {
        return timeNeeded;
    }

    public void setTimeNeeded(int timeNeeded) {
        this.timeNeeded = timeNeeded;
    }

    public List<Ingredient> getIngredientList() {
        return ingredientList;
    }

    public void setIngredientList(List<Ingredient> ingredientList) {
        this.ingredientList = ingredientList;
    }

    /**
     * Methods
     */


    /**
     * Adding Ingriedents to list
     * @param ingredient
     */
    public void addIngredient(Ingredient ingredient){
        ingredientList.add(ingredient);
    }

    public void addIngredient(String name, String category, String quantity){
        ingredientList.add(new Ingredient(name, category, quantity));
    }
}