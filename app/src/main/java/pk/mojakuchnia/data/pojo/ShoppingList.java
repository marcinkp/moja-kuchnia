package pk.mojakuchnia.data.pojo;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by marci on 14.02.2018.
 */

public class ShoppingList {
    private Long id;
    private List<Ingredient>  ingredientList;
    private String name;

    public ShoppingList() {
        ingredientList = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    /*
    Should not be used since setting ID is done by DAO
     */
    public void setId(Long id) {
        this.id = id;
    }

    public List<Ingredient> getIngredientList() {
        return ingredientList;
    }

    public void setIngredientList(List<Ingredient> ingredientList) {
        this.ingredientList = ingredientList;
    }

    public void addIngredient(Ingredient ingredient) {
        ingredientList.add(ingredient);
    }

    public void addAllIngredients(List<Ingredient> ingredientList) {
        this.ingredientList.addAll(ingredientList);
    }

    public void removeIngredient(int i) {
        ingredientList.remove(i);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
