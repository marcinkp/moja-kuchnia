package pk.mojakuchnia.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pk.mojakuchnia.R;
import pk.mojakuchnia.data.db.UserProductDao;
import pk.mojakuchnia.data.pojo.Product;

public class ShowMyProducts extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar=null;
    private List<Product> productList;
    private UserProductDao userProductDao;
    private ArrayList<String> productsNameFound;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_my_products);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        productsNameFound = new ArrayList<String>();
        userProductDao = new UserProductDao(this);
        ListView list = (ListView) findViewById(R.id.listOfUserProducts);
        productList = userProductDao.getAllProducts();
        if(productList.isEmpty()) Toast.makeText(this, "Nie posiadasz żadnych wprowadzonych produktów", Toast.LENGTH_SHORT).show();
        else {
            int i = 1;
            for (Product product : productList) {
                productsNameFound.add(i + ". " + product.getProductName());
                i++;
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, productsNameFound);

            list.setAdapter(adapter);
        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.show_my_products, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_deleteProducts) {
            UserProductDao userProductDao = new UserProductDao(this);
            userProductDao.deleteAllProducts();
            userProductDao.close();
            finish();
            startActivity(getIntent());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id){

            case R.id.nav_home:
                Intent home= new Intent(ShowMyProducts.this,Home.class);
                startActivity(home);
                break;
            case R.id.nav_addProductByScanner:
                Intent scanner= new Intent(ShowMyProducts.this,AddProductByScanner.class);
                startActivity(scanner);
                break;
            case R.id.nav_addProductManually:
                Intent manual= new Intent(ShowMyProducts.this,AddProductManually.class);
                startActivity(manual);
                break;
            case R.id.nav_searchRecipesByProducts:
                Intent byProducts= new Intent(ShowMyProducts.this,SearchRecipesByProducts.class);
                startActivity(byProducts);
                break;
            case R.id.nav_showMyProducts:
                Intent showProducts= new Intent(ShowMyProducts.this,ShowMyProducts.class);
                startActivity(showProducts);
                break;
            case R.id.nav_shopingListViev:
                Intent shopingList= new Intent(ShowMyProducts.this,ShoppingListsView.class);
                startActivity(shopingList);
                break;
            case R.id.nav_searchRecipesByText:
                Intent byText= new Intent(ShowMyProducts.this,SearchRecipesByText.class);
                startActivity(byText);
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
