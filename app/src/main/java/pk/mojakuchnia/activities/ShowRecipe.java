package pk.mojakuchnia.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pk.mojakuchnia.R;
import pk.mojakuchnia.data.db.RecipeDao;
import pk.mojakuchnia.data.db.ShoppingListDao;
import pk.mojakuchnia.data.pojo.Ingredient;
import pk.mojakuchnia.data.pojo.Recipe;
import pk.mojakuchnia.data.pojo.ShoppingList;

public class ShowRecipe extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar=null;
    private Recipe recipeClicked = new Recipe();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_recipe);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String name = getIntent().getStringExtra("RecipeName");
        RecipeDao recipeDao = new RecipeDao(this);
        ArrayList<Recipe> allRecipes = (ArrayList<Recipe>) recipeDao.getAllRecipes();

        for(Recipe recipe: allRecipes){
                    if(name.equals(recipe.getRecipeName())){
                        recipeClicked = recipe;
                        break;
                    }
         }

        TextView nameRecipeTextViev = (TextView) findViewById(R.id.nazwaTextViev);
        nameRecipeTextViev.setText(recipeClicked.getRecipeName());
        TextView DescriptionRecipeTextViev = (TextView) findViewById(R.id.descriptionRecipeTextView);
        DescriptionRecipeTextViev.setText(recipeClicked.getRecipe());
        ListView listView = (ListView) findViewById(R.id.ingredientsRecipeListViev);
        ArrayList<String> ingredientNamesList = new ArrayList<String>();

        for(Ingredient ingredient:recipeClicked.getIngredientList()){
            ingredientNamesList.add(ingredient.getName()+" "+ingredient.getQuantity());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,ingredientNamesList);
        listView.setAdapter(adapter);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.show_recipe, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_addToShoppingList) {
            ShoppingList shoppingList = new ShoppingList();
            shoppingList.setIngredientList(recipeClicked.getIngredientList());
            shoppingList.setName(recipeClicked.getRecipeName()+" shopping list");
            ShoppingListDao shoppingListDao = new ShoppingListDao(this);
            shoppingListDao.insertShoppingList(shoppingList);
            shoppingListDao.close();
            Toast.makeText(this, "Dodano liste zakupowa", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id){

            case R.id.nav_home:
                Intent home= new Intent(ShowRecipe.this,Home.class);
                startActivity(home);
                break;
            case R.id.nav_addProductByScanner:
                Intent scanner= new Intent(ShowRecipe.this,AddProductByScanner.class);
                startActivity(scanner);
                break;
            case R.id.nav_addProductManually:
                Intent manual= new Intent(ShowRecipe.this,AddProductManually.class);
                startActivity(manual);
                break;
            case R.id.nav_searchRecipesByProducts:
                Intent byProducts= new Intent(ShowRecipe.this,SearchRecipesByProducts.class);
                startActivity(byProducts);
                break;
            case R.id.nav_showMyProducts:
                Intent showProducts= new Intent(ShowRecipe.this,ShowMyProducts.class);
                startActivity(showProducts);
                break;
            case R.id.nav_shopingListViev:
                Intent shopingList= new Intent(ShowRecipe.this,ShoppingListsView.class);
                startActivity(shopingList);
                break;
            case R.id.nav_searchRecipesByText:
                Intent byText= new Intent(ShowRecipe.this,SearchRecipesByText.class);
                startActivity(byText);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
