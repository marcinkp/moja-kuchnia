package pk.mojakuchnia.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import pk.mojakuchnia.R;
import pk.mojakuchnia.data.db.UserProductDao;
import pk.mojakuchnia.data.pojo.Product;

public class AddProductManually extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar=null;
    private Button dodajProdukt;
    private String kategoriaProduktu;
    private String nazwaProduktu;
    private String opisProduktu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product_manually);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        Button dodajProdukt = (Button) findViewById(R.id.addProductButton);
        Spinner kategorieSpinner = (Spinner) findViewById(R.id.kategorieSpinner);
        kategoriaProduktu = null;

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.Kategorie,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        kategorieSpinner.setAdapter(adapter);
        kategorieSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TextView textView = (TextView) view;
                String wybranaKategoria = textView.getText().toString();
                if(wybranaKategoria.equals("Brak kategorii")) {
                    kategoriaProduktu = null;
                }
                else kategoriaProduktu = wybranaKategoria;


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        dodajProdukt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addProduct();
            }
        });


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void addProduct(){
        EditText opisEditText = (EditText) findViewById(R.id.opisEditText);
        EditText nazwaEditText = (EditText) findViewById(R.id.nazwaEditText);
        nazwaProduktu = nazwaEditText.getText().toString();
        opisProduktu = opisEditText.getText().toString();

        Product product = new Product();
        product.setProductName(nazwaProduktu);
        product.setDescription(opisProduktu);
        product.setCategory(kategoriaProduktu);
        //Toast.makeText(this, "Name: " + nazwaProduktu + " Desc: " + opisProduktu + " Category: " + kategoriaProduktu, Toast.LENGTH_LONG).show();
        UserProductDao userProductDao = new UserProductDao(this);
        userProductDao.insertProduct(product);
        userProductDao.close();

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id=item.getItemId();
        switch (id){

            case R.id.nav_home:
                Intent home= new Intent(AddProductManually.this,Home.class);
                startActivity(home);
                break;
            case R.id.nav_addProductByScanner:
                Intent scanner= new Intent(AddProductManually.this,AddProductByScanner.class);
                startActivity(scanner);
                break;
            case R.id.nav_addProductManually:
                Intent manual= new Intent(AddProductManually.this,AddProductManually.class);
                startActivity(manual);
                break;
            case R.id.nav_searchRecipesByProducts:
                Intent byProducts= new Intent(AddProductManually.this,SearchRecipesByProducts.class);
                startActivity(byProducts);
                break;
            case R.id.nav_showMyProducts:
                Intent showProducts= new Intent(AddProductManually.this,ShowMyProducts.class);
                startActivity(showProducts);
                break;
            case R.id.nav_shopingListViev:
                Intent shopingList= new Intent(AddProductManually.this,ShoppingListsView.class);
                startActivity(shopingList);
                break;
            case R.id.nav_searchRecipesByText:
                Intent byText= new Intent(AddProductManually.this,SearchRecipesByText.class);
                startActivity(byText);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
