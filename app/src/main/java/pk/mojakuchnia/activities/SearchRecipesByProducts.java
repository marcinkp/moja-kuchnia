package pk.mojakuchnia.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pk.mojakuchnia.R;
import pk.mojakuchnia.data.db.RecipeDao;
import pk.mojakuchnia.data.db.ShoppingListDao;
import pk.mojakuchnia.data.db.UserProductDao;
import pk.mojakuchnia.data.pojo.Product;
import pk.mojakuchnia.data.pojo.Recipe;
import pk.mojakuchnia.data.pojo.ShoppingList;

public class SearchRecipesByProducts extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar=null;
    private ListView listView;
    private List<Product> productList;
    private UserProductDao userProductDao;
    private ArrayList<String> productsNamesFound;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_recipes_by_products);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        productsNamesFound = new ArrayList<String>();
        userProductDao = new UserProductDao(this);
        productList = userProductDao.getAllProducts();
        if(productList.isEmpty()) {
            Toast.makeText(this, "Nie posiadasz żadnych wprowadzonych produktów", Toast.LENGTH_SHORT).show();
            finish();
            startActivity(new Intent(SearchRecipesByProducts.this,Home.class));
        }
        else {
            for (Product product : productList) {
                productsNamesFound.add(product.getProductName());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_checked,productsNamesFound);
            Button searchButton = (Button) findViewById(R.id.searchByProductsButton);
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    search();
                }
            });
            listView = (ListView) findViewById(R.id.listOfToMarkUserProducts);
            listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            listView.setTextFilterEnabled(true);
            listView.setAdapter(adapter);
        }



        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    private void search() {
        Intent intent = new Intent(this, ShowRecipes.class);
        EditText prefTimeEditText = (EditText) findViewById(R.id.prepTimeBuProductsEditText);
        String prefTime = prefTimeEditText.getText().toString();
        if(prefTime.equals("")) prefTime="0";
        UserProductDao userProductDao = new UserProductDao(this);
        ArrayList<Product>  productList = (ArrayList<Product>) userProductDao.getAllProducts();
        userProductDao.close();
        ArrayList<Product> tofindRecipeProductList = new ArrayList<Product>();
        for(int k=0;k<listView.getCount();k++){
            if(listView.isItemChecked(k)){
                for(Product product:productList){
                    if(listView.getItemAtPosition(k).equals(product.getProductName())){
                        tofindRecipeProductList.add(product);
                        break;
                    }
                }
            }
        }

        RecipeDao recipeDao = new RecipeDao(this);
        List<Recipe> recipeList = recipeDao.getByListOfProducts(tofindRecipeProductList, Integer.valueOf(prefTime));
        recipeDao.close();

        ArrayList<String> recipesNamesFound = new ArrayList<String>();
        String toDisp = "";
        int i=1;
        for (Recipe returnedRecipe : recipeList) {
            toDisp += returnedRecipe.getRecipeName() + "\n-----\n";
            recipesNamesFound.add(i+". "+returnedRecipe.getRecipeName());
            i++;
        }
        Toast.makeText(this, toDisp, Toast.LENGTH_LONG).show();
        intent.putExtra("recipesNameFound", recipesNamesFound);
        startActivity(intent);

    }

    private void searchOnAll(){
        Intent intent = new Intent(this, ShowRecipes.class);
        UserProductDao userProductDao = new UserProductDao(this);
        List<Product> productList = userProductDao.getAllProducts();
        userProductDao.close();

        RecipeDao recipeDao = new RecipeDao(this);
        List<Recipe> recipeList = recipeDao.getByListOfProducts(productList, 0);
        recipeDao.close();
        ArrayList<String> recipesNamesFound = new ArrayList<String>();
        String toDisp = "";
        int i=1;
        for (Recipe returnedRecipe : recipeList) {
            toDisp += returnedRecipe.getRecipeName() + "\n-----\n";
            recipesNamesFound.add(i+". "+returnedRecipe.getRecipeName());
            i++;
        }
        Toast.makeText(this, toDisp, Toast.LENGTH_LONG).show();
        intent.putExtra("recipesNameFound", recipesNamesFound);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_recipes_by_products, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_markALL) {
            for ( int i=0; i < listView.getAdapter().getCount(); i++) {
                listView.setItemChecked(i, true);
            }
            return true;
        }
        if (id == R.id.action_unmarkALL) {
            for ( int i=0; i < listView.getAdapter().getCount(); i++) {
                listView.setItemChecked(i, false);
            }
            return true;
        }
        else if (id == R.id.action_searchByAll) {
            searchOnAll();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id){

            case R.id.nav_home:
                Intent home= new Intent(SearchRecipesByProducts.this,Home.class);
                startActivity(home);
                break;
            case R.id.nav_addProductByScanner:
                Intent scanner= new Intent(SearchRecipesByProducts.this,AddProductByScanner.class);
                startActivity(scanner);
                break;
            case R.id.nav_addProductManually:
                Intent manual= new Intent(SearchRecipesByProducts.this,AddProductManually.class);
                startActivity(manual);
                break;
            case R.id.nav_searchRecipesByProducts:
                Intent byProducts= new Intent(SearchRecipesByProducts.this,SearchRecipesByProducts.class);
                startActivity(byProducts);
                break;
            case R.id.nav_showMyProducts:
                Intent showProducts= new Intent(SearchRecipesByProducts.this,ShowMyProducts.class);
                startActivity(showProducts);
                break;
            case R.id.nav_shopingListViev:
                Intent shopingList= new Intent(SearchRecipesByProducts.this,ShoppingListsView.class);
                startActivity(shopingList);
                break;
            case R.id.nav_searchRecipesByText:
                Intent byText= new Intent(SearchRecipesByProducts.this,SearchRecipesByText.class);
                startActivity(byText);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
