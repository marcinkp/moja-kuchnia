package pk.mojakuchnia.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pk.mojakuchnia.R;
import pk.mojakuchnia.data.db.RecipeDao;
import pk.mojakuchnia.data.pojo.Recipe;

public class SearchRecipesByText extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar=null;
    EditText editText;
    Button searchButton;
    private List<Recipe> recipeList;
    private ArrayList<String> recipesNameFound;
    private RecipeDao recipeDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_recipes_by_text);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        recipeDao = new RecipeDao(this);
        recipesNameFound = new ArrayList<String>();
        searchButton = (Button) findViewById(R.id.searchButton);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search();
            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void search() {
        Intent intent = new Intent(this, ShowRecipes.class);
        EditText nameIngredientEditText = (EditText) findViewById(R.id.nameIngredientsEdit);
        EditText prepTimeEditText = (EditText) findViewById(R.id.prepTimeEdit);
        String query = nameIngredientEditText.getText().toString();
        String prepTime = prepTimeEditText.getText().toString();
        if(prepTime.equals("")) prepTime="0";
        int timeOfCooking = Integer.valueOf(prepTime);
        Toast.makeText(this, query + " "+ timeOfCooking, Toast.LENGTH_SHORT).show();
        recipeList = recipeDao.getBySearchQuery(query, timeOfCooking);

        String toDisp = "";
        int i=1;
        for (Recipe returnedRecipe : recipeList) {
            toDisp += returnedRecipe.getRecipeName() + "\n-----\n";
            recipesNameFound.add(i+". "+returnedRecipe.getRecipeName());
            i++;
        }
        Toast.makeText(this, toDisp, Toast.LENGTH_SHORT).show();
        intent.putExtra("recipesNameFound", recipesNameFound);
        startActivity(intent);
        recipesNameFound.clear();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id){

            case R.id.nav_home:
                Intent home= new Intent(SearchRecipesByText.this,Home.class);
                startActivity(home);
                break;
            case R.id.nav_addProductByScanner:
                Intent scanner= new Intent(SearchRecipesByText.this,AddProductByScanner.class);
                startActivity(scanner);
                break;
            case R.id.nav_addProductManually:
                Intent manual= new Intent(SearchRecipesByText.this,AddProductManually.class);
                startActivity(manual);
                break;
            case R.id.nav_searchRecipesByProducts:
                Intent byProducts= new Intent(SearchRecipesByText.this,SearchRecipesByProducts.class);
                startActivity(byProducts);
                break;
            case R.id.nav_showMyProducts:
                Intent showProducts= new Intent(SearchRecipesByText.this,ShowMyProducts.class);
                startActivity(showProducts);
                break;
            case R.id.nav_shopingListViev:
                Intent shopingList= new Intent(SearchRecipesByText.this,ShoppingListsView.class);
                startActivity(shopingList);
                break;
            case R.id.nav_searchRecipesByText:
                Intent byText= new Intent(SearchRecipesByText.this,SearchRecipesByText.class);
                startActivity(byText);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
