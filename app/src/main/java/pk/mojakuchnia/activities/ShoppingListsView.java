package pk.mojakuchnia.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import pk.mojakuchnia.R;
import pk.mojakuchnia.data.db.ShoppingListDao;
import pk.mojakuchnia.data.pojo.ShoppingList;

public class ShoppingListsView extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar=null;
    private ArrayList<ShoppingList> listOfShoppingLists;
    private ArrayList<String> listOfShoppingListsNamesFound;
    private ShoppingListDao shoppingListDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_lists_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        listOfShoppingLists = new ArrayList<ShoppingList>();
        listOfShoppingListsNamesFound = new ArrayList<String>();
        shoppingListDao = new ShoppingListDao(this);
        ListView list = (ListView) findViewById(R.id.listOfShoppingLists);
        listOfShoppingLists = (ArrayList<ShoppingList>) shoppingListDao.getAllShoppingLists();

        if(listOfShoppingLists.isEmpty()) Toast.makeText(this, "Nie posiadasz żadnych wprowadzonych list zakupowych", Toast.LENGTH_SHORT).show();
        else {
            for (ShoppingList shopingList : listOfShoppingLists) {
                listOfShoppingListsNamesFound.add(shopingList.getName());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, listOfShoppingListsNamesFound);

            list.setAdapter(adapter);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Object name = adapterView.getItemAtPosition(i);
                    if(name!=null || i!=0) {
                        Intent shoppingListView = new Intent(ShoppingListsView.this, ShoppingListView.class);
                        shoppingListView.putExtra("ShoppingListName", name.toString());
                        startActivity(shoppingListView);
                    }
                }
            });
        }


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id){

            case R.id.nav_home:
                Intent home= new Intent(ShoppingListsView.this,Home.class);
                startActivity(home);
                break;
            case R.id.nav_addProductByScanner:
                Intent scanner= new Intent(ShoppingListsView.this,AddProductByScanner.class);
                startActivity(scanner);
                break;
            case R.id.nav_addProductManually:
                Intent manual= new Intent(ShoppingListsView.this,AddProductManually.class);
                startActivity(manual);
                break;
            case R.id.nav_searchRecipesByProducts:
                Intent byProducts= new Intent(ShoppingListsView.this,SearchRecipesByProducts.class);
                startActivity(byProducts);
                break;
            case R.id.nav_showMyProducts:
                Intent showProducts= new Intent(ShoppingListsView.this,ShowMyProducts.class);
                startActivity(showProducts);
                break;
            case R.id.nav_shopingListViev:
                Intent shopingList= new Intent(ShoppingListsView.this,ShoppingListsView.class);
                startActivity(shopingList);
                break;
            case R.id.nav_searchRecipesByText:
                Intent byText= new Intent(ShoppingListsView.this,SearchRecipesByText.class);
                startActivity(byText);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
