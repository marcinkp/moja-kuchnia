package pk.mojakuchnia.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import pk.mojakuchnia.R;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id=item.getItemId();
        switch (id){

            case R.id.nav_home:
                Intent home= new Intent(Home.this,Home.class);
                startActivity(home);
                break;
            case R.id.nav_addProductByScanner:
                Intent scanner= new Intent(Home.this,AddProductByScanner.class);
                startActivity(scanner);
                break;
            case R.id.nav_addProductManually:
                Intent manual= new Intent(Home.this,AddProductManually.class);
                startActivity(manual);
                break;
            case R.id.nav_searchRecipesByProducts:
                Intent byProducts= new Intent(Home.this,SearchRecipesByProducts.class);
                startActivity(byProducts);
                break;
            case R.id.nav_showMyProducts:
                Intent showProducts= new Intent(Home.this,ShowMyProducts.class);
                startActivity(showProducts);
                break;
            case R.id.nav_shopingListViev:
                Intent shopingList= new Intent(Home.this,ShoppingListsView.class);
                startActivity(shopingList);
                break;
            case R.id.nav_searchRecipesByText:
                Intent byText= new Intent(Home.this,SearchRecipesByText.class);
                startActivity(byText);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
